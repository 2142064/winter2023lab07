import java.util.Scanner;
public class TicTacToeGame{
	public static void main (String[]args){
	System.out.println("Hello!");
	Scanner reader = new Scanner(System.in);
	Board board=new Board();
	boolean gameOver=false;
	int player=1;
	Square playerToken=Square.X;
	
	while(gameOver==false){
		System.out.println(board);
			if(player==1){
				playerToken=Square.X;
			}else{
				playerToken=Square.O;
			}
		System.out.println("Your turn, "+player);
		System.out.println("Give a row");
		int r=reader.nextInt();
		System.out.println("Give a column");
		int c=reader.nextInt();
		board.placeToken(r, c, playerToken);
			while(board.placeToken(r, c, playerToken)==false){
				System.out.println("Give a row");
				r=reader.nextInt();
				System.out.println("Give a column");
				c=reader.nextInt();
				board.placeToken(r, c, playerToken);
			}
			
			if(board.checkIfWinning(playerToken)==true){
				System.out.println("You won, "+player);
				gameOver=true;
			}else if(board.checkIfFull()==true){
				System.out.println("Tie");
				gameOver=true;
			}else{
				player++;
				if(player>2){
					player=1;
				}
			}
		
	}	
	
	}
}