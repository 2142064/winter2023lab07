public class Board{
	private Square[][] tictactoeBoard;
	
	//Constructor
	public Board(){
	this.tictactoeBoard = new Square[3][3];
	
	
	for(int i=0; i<this.tictactoeBoard.length;i++){
		for(int j=0;j<this.tictactoeBoard[i].length;j++){
			this.tictactoeBoard[i][j]=Square.BLANK;
			
		}
	}
	}
	
	public boolean placeToken(int row, int col, Square playerToken){
	if (!(row >=0 && row<this.tictactoeBoard.length)){
		if (!(col >=0 && col<this.tictactoeBoard[row].length)){
			return false;
			}
	}else if(this.tictactoeBoard[row][col]==Square.BLANK){
		this.tictactoeBoard[row][col]=playerToken;
		return true;
	}else{
		return true;
	}
	
		
		return true;
	}
	
	public boolean checkIfFull(){
	for(int i=0; i<this.tictactoeBoard.length;i++){
		for(int j=0;j<this.tictactoeBoard[i].length;j++){
			if(this.tictactoeBoard[i][j]==Square.BLANK){
				return false;
			}
		}
	}
	
		return true;
	}
	
	//Winning condition
	private boolean checkifWinningVertical(Square playerToken){
		for(int i=0; i<this.tictactoeBoard.length;i++){
			if(this.tictactoeBoard[i][0]==playerToken&& this.tictactoeBoard[i][1]==playerToken && this.tictactoeBoard[i][2]==playerToken){
				return true;
		}
	}
	return false;
	}
	
	private boolean checkifWinningHorizontal(Square playerToken){
		for(int i=0; i<this.tictactoeBoard.length;i++){
			if(this.tictactoeBoard[0][i]==playerToken&& this.tictactoeBoard[1][i]==playerToken && this.tictactoeBoard[2][i]==playerToken){
				return true;
		}
	}
	return false;
	}
	
	public boolean checkIfWinning(Square playerToken){
		if(checkifWinningHorizontal(playerToken)==true || checkifWinningVertical(playerToken)==true){
			return true;
		}
		return false;
	}
	
	
	//toString
	public String toString(){
	String s="";
		for(int i=0; i<this.tictactoeBoard.length;i++){
			for(int j=0;j<this.tictactoeBoard[i].length;j++){
			s+=" "+ tictactoeBoard[i][j];
			
		}
		s+=" ";
		s+="\n";
		}
	return s;
	}
	
}